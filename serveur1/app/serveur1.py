from fastapi import FastAPI, HTTPException
import httpx
import asyncio
from fastapi.responses import JSONResponse

server3_address = "http://serveur3:8080"

app1 = FastAPI()

# Endpoint pour s'enregistrer auprès du serveur intermédiaire
@app1.on_event("startup")
async def startup_event():
    async with httpx.AsyncClient() as client:
        response = await client.post(f"{server3_address}/register/server1", json={"server_address": "http://serveur1:4567"})
        response.raise_for_status()


# Adresse du deuxième serveur
server2_address = None
server4_address = None

@app1.get("/")
async def hello():
    return "serveur1"

@app1.get("/send-pong")
async def send_pong():

    global server2_address
    global server4_address

    # Si l'adresse du deuxième serveur n'est pas encore connue, la demander au serveur intermédiaire
    if server2_address is None:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{server3_address}/get_address/server2")
            response.raise_for_status()
            server2_address = response.json()["server_address"]

    if server4_address is None:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{server3_address}/get_address/server4")
            response.raise_for_status()
            server4_address = response.json()["server_address"]

    try:
        # Envoi de la requête "pong"
        async with httpx.AsyncClient() as client:
            response = await client.post(f"{server4_address}/transmit", json={"recipient": server2_address, "action": "pong"})
            response.raise_for_status()
            print("Ping sent successfully from Server 1")
    except httpx.HTTPError as e:
        return JSONResponse(status_code=500, content={"error": str(e)})


# Endpoint pour recevoir des pings du deuxième serveur
@app1.get("/ping")
async def receive_ping():
    try:
        await asyncio.sleep(1)
        print("ping")
        # Envoi de la requête "pong" en réponse à "ping"
        async with httpx.AsyncClient() as client:
            response = await client.post(f"{server4_address}/transmit", json={"recipient": server2_address, "action": "pong"})
            response.raise_for_status()
            return {"message": "ok"}

    except httpx.HTTPError as e:
        return JSONResponse(status_code=500, content={"error": str(e)})