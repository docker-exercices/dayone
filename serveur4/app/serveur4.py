from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
import httpx

app4 = FastAPI()

server3_address = "http://serveur3:8080"

# Endpoint pour s'enregistrer auprès du serveur intermédiaire
@app4.on_event("startup")
async def startup_event():
    async with httpx.AsyncClient() as client:
        response = await client.post(f"{server3_address}/register/server4", json={"server_address": "http://serveur4:1111"})
        response.raise_for_status()

@app4.get("/")
async def hello():
    return "serveur4"

@app4.post("/transmit")
async def transmit_message(data: dict):
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{data['recipient']}/{data['action']}")
            response.raise_for_status()
            print("transmit !")
            return {"message": "ok"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error during transmission: {str(e)}")
